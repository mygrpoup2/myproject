#!/bin/bash

existentes=0
nao_existentes=0

[ -e "$1" ] && existentes=$((existentes+1)) || nao_existentes=$((nao_existentes+1))
[ -e "$2" ] && existentes=$((existentes+1)) || nao_existentes=$((nao_existentes+1))
[ -e "$3" ] && existentes=$((existentes+1)) || nao_existentes=$((nao_existentes+1))
[ -e "$4" ] && existentes=$((existentes+1)) || nao_existentes=$((nao_existentes+1))
[ -e "$5" ] && existentes=$((existentes+1)) || nao_existentes=$((nao_existentes+1))

echo "Quantidade de arquivos existentes: $existentes"
echo "Quantidade de arquivos não existentes: $nao_existentes"
