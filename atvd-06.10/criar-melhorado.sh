#!/bin/bash

A=$((RANDOM % 12 + 2))


if [ -e "${A}.txt" ]; then
	echo -e "\e[31mErro: O arquivo ${A}.txt já existe. \e[0m"
	exit 1
else
	touch "${A}.txt"
	echo "Arquivo ${A}.txt criado" 
fi
