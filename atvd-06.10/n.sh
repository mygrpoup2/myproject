#!/bin/bash

x=$1
y=$2

test -e $x && echo Arquivo Existe || echo Arquivo Não Existe
test -e $y && echo Arquivo Existe || echo Arquivo Não Existe
test -e $x || echo $x Não Existe!
test -e $1 || exit 1

x=$(wc -l < "$x")
y=$(wc -l < "$y")

test $x -gt $y && echo $x || echo $y
