#!/bin/bash

# Informe dois nomes como parâmetros de linha de comando
read -p "Informe o nome: " n1
read -p "Informe novamente: " n2

# Verificar se o  nome é vazio

[ -z "$n1" ] && exit 0
[ -z "$n2" ] && exit 0

# Escrever os nomes em ordem alfabética

[ "$n1" < "$n2" ] && echo $n1 $n2 


