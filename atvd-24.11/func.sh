#!/bin/bash

linha() {

	local num_linha=$1
	local arq=$2

	if [ -e "$arq" ]; then
		sed -n "${num_linha}p" "$arq"
	else
		echo "Arquivo não encontrado: $arq"
	fi

}

col() { 
	local num_col=$1
	local arq=$2

	if [ -e "$arq" ]; then
		cat $arq | cut -d ':' -f $num_col
	else
		echo "Arquivo não encontrado: $arq"
	fi
}
