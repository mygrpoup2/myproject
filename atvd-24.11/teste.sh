#!/bin/bash

source func.sh

read -p "Digite o nome do arquivo: " nome_arquivo


read -p "Escolha entre linhas (L) ou colunas (C): " opcao

opcao=$(echo "$opcao" | tr '[:lower:]' '[:upper:]')

case "$opcao" in
    L)
        read -p "Digite o número da linha: " numero_linha
        linha "$numero_linha" "$nome_arquivo"
        ;;
    C)
        read -p "Digite o número da coluna: " numero_coluna
        col "$numero_coluna" "$nome_arquivo"
        ;;
    *)
        echo "Opção inválida. Escolha entre L ou C."
        ;;
esac

