#!/bin/bash

cam="/home/ifpb"

if [ "USER" != "ifpb" ]; then
	echo "Deve ser executado pelo usuário - ifpb - "
	exit 1
fi

if [ $# -eq 1 ]; then
	cam=$1
fi

if [ ! -e "cam" ]; then
	echo "O caminho '$cam' não existe."
	exit 1
fi 

arq=$(find "cam" -maxdepth 1 -type f | wc -l)
pas=$(find "cam" -maxdepth 1 -type d | wc -l)

echo "Total de arquivos: $arq"
echo "Total de pastas: $pas"

