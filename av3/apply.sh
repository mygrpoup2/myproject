#!/bin/bash
source func.sh

opcao=$1

case $opcao in
  1) echo "Escolha uma cor para o prompt:"
     echo "preto, vermelho, verde, amarelo, azul, magenta, ciano, branco"
    read -p "Digite o nome da cor: " cor

    PS1="$(cor $cor)$PS1$(cor)"
    ;;
  2) PS1="$(usuario)@$(cor vermelho)\$$(cor) "
    ;;
  3) PS1="$(duas_linhas)"
    ;;
  4) # Sair
    # Não faz nada
    ;;
  *) echo "Opção inválida. Tente novamente."
    ;;

esac


