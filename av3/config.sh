#!/bin/bash

source func.sh

echo "Escolha uma opção de customização do PS1:"
echo "1) Mudar a cor do prompt"
echo "2) Exibir apenas o nome do usuário"
echo "3) Prompt em duas linhas"
echo "4) Sair"
read -p "Digite o número da opção: " opcao


./apply.sh $opcao

