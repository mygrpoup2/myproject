#!/bin/bash


cor() {
  case $1 in
    preto) echo "\e[0;30m";;
    vermelho) echo "\e[0;31m";;
    verde) echo "\e[0;32m";;
    amarelo) echo "\e[0;33m";;
    azul) echo "\e[0;34m";;
    magenta) echo "\e[0;35m";;
    ciano) echo "\e[0;36m";;
    branco) echo "\e[0;37m";;
    *) echo "\e[0m";; # cor padrão
  esac
}

usuario() {
  echo "$USER"
}

duas_linhas() {
  echo "$PS1\n\$ "
}


