#!/bin/bash

source func.sh

if [ -z "$color" ]; then
	echo "Erro: Execute primeiro o script config.sh para escolhar a customização."
	exit 1
fi

customiza $color $(whoami) $(hostname) $(pwd)

echo "Customização aplicada com sucesso!"
