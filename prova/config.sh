#!/bin/bash

source func.sh

echo "Escolha uma customização:"
echo "1. Verde ( Padrão )"
echo "2. Azul"
echo "3. Vermelho"
read -p "Selecione a opção desejada: " escolha

case $escolha in 
	2) color="0;34"
		;;
	3) color="0;31"
		;;
	*) color="0;32"
		;;
esac

customiza $color $(whoami) $(hostname) $(pwd)
