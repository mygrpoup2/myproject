#!/bin/bash

customiza() {
	local color=$1
	local user=$2
	local host=$3
	local path=$4

	PS1="[\e[${color}m\]${user}@${host}:\w\$\[\e[0m\]"
}

resetar() {
	PS1='\[\e[0;32m\]\u@\h\[\e[0m\]:\[\e[0;34m\]\w\[\e[0m\]\$'
}
